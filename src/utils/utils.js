import { chunk } from 'lodash'
export function arrLt (a, b) {
  let eqNum = 0
  const size = Math.min(a.length, b.length)
  for (let i = 0; i < size; i++) {
    if (a[i] > b[i]) return false
    if (a[i] === b[i]) eqNum++
  }
  return eqNum !== size
}

export function arrLtOne (a, b) {
  const size = Math.min(a.length, b.length)
  for (let i = 0; i < size; i++) {
    if (a[i] < b[i]) return true
  }
}

export function arrGt (a, b) {
  let eqNum = 0
  const size = Math.min(a.length, b.length)
  for (let i = 0; i < size; i++) {
    if (a[i] < b[i]) return false
    if (a[i] === b[i]) eqNum++
  }
  return eqNum !== size
}

export function arrGtOne (a, b) {
  const size = Math.min(a.length, b.length)
  for (let i = 0; i < size; i++) {
    if (a[i] > b[i]) return true
  }
}

export function arrLte (a, b) {
  for (let i = 0; i < Math.min(a.length, b.length); i++) {
    if (a[i] > b[i]) return false
  }
  return true
}

export function arrGte (a, b) {
  for (let i = 0; i < Math.min(a.length, b.length); i++) {
    if (a[i] <= b[i]) return false
  }
  return true
}

export function arrSub (a, b) {
  return a.map((x, i) => a[i] - b[i])
}

export function arrAdd (a, b) {
  return a.map((x, i) => a[i] + b[i])
}

export function createMatrix (m, n) {
  return chunk(new Array(m * n).fill(0), n)
}

export function arrEq (a, b) {
  const size = Math.min(a.length, b.length)
  for (let i = 0; i < size; i++) {
    if (a[i] !== b[i]) return false
  }
  return true
}
