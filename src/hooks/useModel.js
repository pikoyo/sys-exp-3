import { ref, watch } from 'vue'

/**
 * 对 model 类型数据的访问和更新做封装
 * 这个函数会使用初值创建一个新的 ref，无论更新事件是否正确触发，都会 **在内部保存一份最新的数据**
 * 当未使用 v-model或者未绑定 `@update:xxx` 事件时，修改返回的 ref 仍然有效
 * @param props 组件的 props 对象
 * @param key 组件 props 的 key
 * @param emit 组件的 emit 函数
 * @returns 对 model 数据的引用
 */
export const useModel = (
  props,
  key,
  emit
) => {
  /**
   * 当前是否在从 props 同步数据
   * 避免同步数据时执行 update:xxx 回调
   */
  let isSync = false
  const modelRef = ref(props[key])
  watch(
    () => props[key],
    (val) => {
      isSync = true
      modelRef.value = val
      isSync = false
    }
  )
  watch(
    modelRef,
    (val) => {
      if (!isSync) emit('update:' + key, val)
    },
    {
      immediate: true
    }
  )
  return modelRef
}
