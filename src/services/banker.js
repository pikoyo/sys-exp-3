import {
  createMatrix,
  arrGtOne,
  arrLte,
  arrSub,
  arrAdd,
  arrLtOne,
  arrEq
} from '../utils/utils'
import {
  random,
  cloneDeep,
  clone
} from 'lodash'
/**
 * 创建一个银行家
 * @param {*} resourceCount 资源个数
 * @param {*} processCount 进程个数
 * @returns
 */
export function createBanker (resourceCount, processCount) {
  /**
   * 可用资源
   */
  const available = new Array(resourceCount).fill(0)
  /**
   * 最大需求量
   */
  const max = createMatrix(processCount, resourceCount)
  /**
   * 分配资源量
   */
  const allocation = createMatrix(processCount, resourceCount)

  return {
    available,
    max,
    allocation,
    resourceCount,
    processCount
  }
}

/**
 * 初始化一个随机的银行家
 * @param {*} banker
 * @param {*} maxResourceCount
 */
export function initRandomBanker (banker, maxResourceCount = 10) {
  /** 控制max超出范围的概率 */
  const maxRate = Math.pow(0.5, 1 / banker.processCount / banker.resourceCount)
  const maxValue = parseInt(maxResourceCount / maxRate)

  for (let i = 0; i < banker.resourceCount; i++) {
    banker.available[i] = random(1, maxResourceCount)
  }
  for (let i = 0; i < banker.processCount; i++) {
    for (let j = 0; j < banker.resourceCount; j++) {
      /* 降低概率 */
      banker.max[i][j] = random(0, maxValue)
      banker.allocation[i][j] = random(0, banker.max[i][j])
    }
  }
  return banker
}

/**
 * 计算 need 数组
 * @param {*} banker
 */
export function getNeed (banker) {
  const need = createMatrix(banker.processCount, banker.resourceCount)
  for (let i = 0; i < need.length; i++) {
    for (let j = 0; j < need[i].length; j++) {
      need[i][j] = banker.max[i][j] - banker.allocation[i][j]
    }
  }
  return need
}

/**
 * 复制银行家
 * @param {*} banker
 * @returns
 */
export function copyBanker (banker) {
  return cloneDeep(banker)
}

function printSeq (msg, work, banker, need) {
  console.group()
  console.log(msg)
  console.log('work')
  console.table([work])
  console.log('max')
  console.table(banker.max)
  console.log('allocation')
  console.table(banker.allocation)
  console.log('need')
  console.table(need)
  console.groupEnd()
}

/**
 * 计算安全银行家算法的安全序列
 * @param {*} banker
 * @returns 如果有安全序列，返回安全序列，否则返回 null
 */
export function getSafeSeq (banker) {
  let work = cloneDeep(banker.available)
  const need = getNeed(banker)
  let hasTried = true
  const seq = []
  // printSeq('开始分配', work, banker, need)
  while (true) {
    hasTried = false
    for (let i = 0; i < banker.processCount; i++) {
      const isSatisfied = seq.indexOf(i) === -1 && arrLte(need[i], work)

      if (!isSatisfied) { continue }

      work = work.map((x, j) => x + banker.allocation[i][j])
      seq.push(i)
      // printSeq('添加:' + i, work, banker, need)
      /* 如果安全序列已找齐，则返回安全序列 */
      if (seq.length === banker.processCount) return seq
      hasTried = true
    }

    /**
     * 没有任何进程可以申请资源，返回空
     */
    if (!hasTried) {
      // printSeq('无进程可分配', work, banker)
      return null
    }
  }
}

/**
 *
 * @param {*} banker 银行家
 * @param {*} i 进程id
 * @param {*} nums 进程需要申请的资源数组
 */
export function tryApplyResource (banker, i, nums) {
  if (nums.length !== banker.resourceCount) {
    return {
      status: false,
      msg: '资源个数错误'
    }
  }

  if (arrLtOne(arrSub(banker.max[i], banker.allocation[i]), nums)) {
    return {
      status: false,
      msg: '资源申请不合理'
    }
  }

  if (arrGtOne(nums, banker.available)) {
    return {
      status: false,
      msg: '资源申请超过最大可用资源数，资源不够分配'
    }
  }

  /* 尝试分配，检测当前是否处于安全状态 */

  const cache = [
    clone(banker.available),
    clone(banker.allocation[i])
  ]
  banker.available = arrSub(banker.available, nums)
  banker.allocation[i] = arrAdd(banker.allocation[i], nums)

  const safeSeq = getSafeSeq(banker)

  /* 找到安全序列 */
  if (safeSeq) {
    return {
      safeSeq,
      status: true
    }
  }

  /* 恢复现场 */
  banker.available[i] = cache[0]
  banker.allocation[i] = cache[1]

  return {
    status: false,
    msg: '找不到安全序列，进程资源申请不予满足'
  }
}

/**
 * 释放可以释放资源
 * @param {*} banker 银行家结构
 * @param {*} i 进程编号
 */
export function canReleaseResource (banker, i) {
  return arrEq(banker.max[i], banker.allocation[i])
}

/**
 * 释放资源
 * @param {*} banker 银行家结构
 * @param {*} i 进程编号
 */
export function releaseResource (banker, i) {
  banker.available = banker.available.map((x, j) => {
    const result = x + banker.allocation[i][j]
    banker.allocation[i][j] = 0
    banker.max[i][j] = 0
    return result
  })
}
